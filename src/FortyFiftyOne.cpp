#include "FortyFiftyOne.h"

void FortyFiftyOne::pinSetup(uint32_t ch, uint32_t sig, uint32_t mode, uint32_t ctrl_a)
{
    // get the number of selectors
    selectors = 1;
    while (ch != 2) {
        selectors++;
        ch = ch / 2;
    }
    selectionPins = new uint32_t [selectors];

    // get number of signal pins
    if ((channels == 16) || (channels == 8)) {
        signals = 1;
    }
    else if (channels == 4) {
        signals = 2;
    }
    else {
        signals = 4;
    }

    signalPins = new uint32_t [signals];
    for (uint32_t i = 0; i < signals; i++) {
        signalPins[i] = sig + i;
    }

    if ((mode == OUTPUT) || (mode == INPUT) ||
        (mode == INPUT_PULLUP) || (mode == INPUT_PULLDOWN))
    {
        for (uint32_t i = 0; i < selectors; i++)
        {
            selectionPins[i] = ctrl_a + i;
            pinMode(selectionPins[i], OUTPUT);
        }
        for (uint32_t i = 0; i < signals; i++) {
            pinMode(signalPins[i], mode);
        }

    }
}

FortyFiftyOne::FortyFiftyOne(uint32_t signal, uint32_t signal_mode,
                             uint32_t signal_type, uint32_t chan_num) :
                             type(signal_type), enablePin(UNDEFINED),
                             channels(chan_num)
{
    pinSetup(channels, signal, signal_mode, DEFAULT_SELECT);
    setChan(0);
    setEnabled();
}

FortyFiftyOne::FortyFiftyOne(uint32_t signal, uint32_t signal_mode,
                             uint32_t signal_type, uint32_t chan_num,
                             uint32_t enable) :
                             type(signal_type), enablePin(enable),
                             channels(chan_num)
{
    pinSetup(channels, signal, signal_mode, DEFAULT_SELECT);
    setChan(0);
    pinMode(enablePin, OUTPUT);
    setEnabled();
}

FortyFiftyOne::FortyFiftyOne(uint32_t signal, uint32_t signal_mode,
                             uint32_t signal_type, uint32_t ctrl_a, uint32_t chan_num,
                             uint32_t enable) :
                             type(signal_type),
                             enablePin(enable),
                             channels(chan_num)
{
    pinSetup(channels, signal, signal_mode, ctrl_a);
    setChan(0);
    pinMode(enablePin, OUTPUT);
    setEnabled();
}

uint32_t FortyFiftyOne::read(uint32_t chan)
{
    setChan(chan);
    uint32_t value = 0;
    uint32_t pin = 0;
    delayMicroseconds(SETTLE_DELAY); // Let the select settle
    switch (type)
    {
        case ANALOG:
            for (uint32_t i = 0; i < signals; i++) {
                pin = analogRead(signalPins[i]);
                value = value | (pin << (i * 8));
            }
            break;
        case DIGITAL:
            for (uint32_t i = 0; i < signals; i++) {
                pin = digitalRead(signalPins[i]);
                value = value | (pin << (i * 8));
            }
            break;
        default:
            value = UNDEFINED;
            break;
    }
    return value;
}

//bool FortyFiftyOne::write(uint32_t data, uint32_t chan)
//{
//    if (chan != UNDEFINED) setChan(chan);
//
//    switch (type)
//    {
//        case ANALOG:
//            analogWrite(signalPin, data);
//            return true;
//        case DIGITAL:
//            digitalWrite(signalPin, data);
//            return true;
//    }
//    return false;
//}

bool FortyFiftyOne::setChan(uint32_t chan)
{
    if ((curr_ch != chan) && (chan < channels)) {
        curr_ch = chan;
        digitalWrite(selectionPins[0], chan & 0b00000001);
        digitalWrite(selectionPins[1], chan & 0b00000010);
        digitalWrite(selectionPins[2], chan & 0b00000100);
        return true;
    }
    return false;
}

bool FortyFiftyOne::setEnabled(bool val)
{
    if (enablePin >= 0) {
        if ((val) && (!enabled))
        {
            enabled = true;
            digitalWrite(enablePin, HIGH);
            return true;
        }
        else if ((!val) && (enabled))
        {
            enabled = false;
            digitalWrite(enablePin, LOW);
            return true;
        }
    }
    return false;
}
