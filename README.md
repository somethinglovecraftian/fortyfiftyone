# FortyFiftyOne

FortyFiftyOne is a simple library to interface Arduino type boards with the
commonly available 4051 mux/demux chip. It has been tested on Teensy 3.5 and
Arduino Uno with Teensy 4.0 support coming very sooon. It is largely based upon the
[arduino-ad-mux-lib](https://github.com/stechio/arduino-ad-mux-lib/tree/master/src)
library. Since we are mainly using the 4051, the code has been simplified to be
more straightforward since it will not need to cover a wider range of muxes.

## Usage

There are three constructors available to use for this library. To create a mux object with the default settings, use the following constructor:

```c++
FortyFiftyOne mux(6, INPUT, DIGITAL);
```

This will create a multiplexer object that listens for a signal on Digital pin 6. The first parameter is the signal pin that you will be reading from or writing
to. The second denotes whether the signal pin will be an INPUT or OUTPUT, and
the third parameter denotes wheter the input is DIGITAL or ANALOG. The select
pins in this constructor default for 8, 9, and 10.

If you are using a number of multiplexers, it may be worthwhile to use the
inhibit pin on the chips to enable and disable the muxes programatically. You
could then use the same three select pins and the same input/output pin with
a different Arduino pin going to each chip's inhibit pin. The code may look
like this:

```c++
FortyFiftyOne mux_one(6, OUTPUT, DIGITAL, 3);
FortyFiftyOne mux_two(6, OUTPUT, DIGITAL, 4);
```

This will create two objects that both use the default 8, 9, and 10 pins for
the selectors; digital pin 6 as the output value; and digital pins 3 and 4 to
turn each mux on and off. You can use the `setEnabled()` method to turn the mux
chips on and off.

Finally, you may wish to use different pins for the selector pins. If so, a
full constructor looks like this:

```c++
FortyFiftyOne mux(6, INPUT, ANALOG, 3, 4, 5, UNDEFINED);
```

In this example, digital pins 3, 4, and 5 will be used for s0, s1, s2 on the mux
respectively. The `UNDEFINED` value sets the inhibit pin to undefined. You could
easily use a number here to specify a pin to enable/disable the mux on.

[comment]: # This is just a test line to see if I can do something
