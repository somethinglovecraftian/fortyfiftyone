#ifndef FORTYFIFTYONE_H
#define FORTYFIFTYONE_H

#include <Arduino.h>
#include <array>

const uint32_t ANALOG = 0;
const uint32_t DIGITAL = 1;
const uint32_t DEFAULT_SELECT = 8;
const uint32_t DEFAULT_SIGNAL = 5;
const int32_t UNDEFINED = -1;

// Some MCUs run very fast and will move on in iterations
// before it has a chance to properly read the pin. This adds a
// small delay time depending on the MCU.
#ifdef __IMXRT1062__
    const uint32_t SETTLE_DELAY = 5;
#else
    const uint32_t SETTLE_DELAY = 0;
#endif

// masks that are used on return
#define SIGNALS_ 0xFFFFFFFF
#define SIGNAL_1_MASK 0x000000FF
#define SIGNAL_2_MASK 0X0000FF00
#define SIGNAL_3_MASK 0x00FF0000
#define SIGNAL_4_MASK 0xFF000000



class FortyFiftyOne {
private:
    uint32_t curr_ch{};
    uint32_t selectors{};
    uint32_t channels{};
    uint32_t signals{};
    bool enabled{};
    uint32_t enablePin;
    uint32_t *signalPins{};
    uint32_t *selectionPins{};
    uint32_t type;

    void pinSetup(uint32_t ch,
            uint32_t sig,
            uint32_t mode,
            uint32_t ctrl_a);

public:
    /** Partial Constructor

        Creates a 4051 mux object. Assumes the control pins are set up in the
        following scheme:

        CTRL_A      = digital pin 8
        CTRL_B      = digital pin 9
        CTRL_C      = digital pin 10
        enablePin   = UNDEFINED

        uint32_t signal      : Which pin on which the signal is being
                              read/written
        uint32_t signal_mode : Whether the signal is OUTPUT, INPUT, INPUT_PULLUP,
                               or INPUT_PULLDOWN
        uint32_t signal_type      : Whether the signal is ANALOG or DIGITAL
    */

    FortyFiftyOne(uint32_t signal,
                  uint32_t signal_mode,
                  uint32_t signal_type,
                  uint32_t chan_num
              );
    /** Partial Constructor

      Creates a 4051 mux object with an enable pin. Assumes the control pins are
      set up in the following scheme:

      CTRL_A      = digital pin 8
      CTRL_B      = digital pin 9
      CTRL_C      = digital pin 10

      uint32_t signal      : Which pin on which the signal is being
                            read/written
      uint32_t signal_mode : Whether the signal is OUTPUT or INPUT
      uint32_t signal      : Whether the signal is ANALOG or DIGITAL
      uint32_t enable      : Selects a pin for the enablePin
    */

    FortyFiftyOne(uint32_t signal,
                uint32_t signal_mode,
                uint32_t signal_type,
                uint32_t chan_num,
                uint32_t enable
            );

    /** Full Constructor

      Works the same as the partial constructor, but allows for the control pins
      to be explicitly set.

      uint32_t signal        : Which pin on which the signal is being
                              read/written
      uint32_t signal_mode   : Whether the signal is OUTPUT or INPUT
      uint32_t signal        : Whether the signal is ANALOG or DIGITAL
      uint32_t ctrl_a        : Sets the output pin for Select pin A, Select
                               pins B and C will be n+1 and n+2, respectively

    */

    FortyFiftyOne(uint32_t signal,
                  uint32_t signal_mode,
                  uint32_t signal_type,
                  uint32_t ctrl_a,
                  uint32_t chan_num,
                  uint32_t enable
              );
    /** read()

      Reads from the mux either from the current channel or from a specified
      channel.

      uint32_t chan:     If specified, method will read from that channel. Other-
                        wise, reads from the specified channel.

    */

    uint32_t read(uint32_t chan = UNDEFINED);

    /** write()

        Writes to the mux on the current channel or to a specified channel.

        uint32_t chan:   If specified, method will write to that channel. Other-
                        wise, writes to the specified channel.
        uint32_t data:   Data to write to the channel.
    */

    bool write(uint32_t data, uint32_t chan = UNDEFINED);

    /** setChan()

      Sets the channel to be read or written to. If chan is not specified in
      either the read() or write() methods, the previously set chan will be
      used.

      uint32_t chan:     Channel to be set.
    */

    bool setChan(uint32_t chan);
    /** enable()

      Enables the mux object. This can be used to daisy chain multiple 4051
      chips to read any multiple of chips.

      No parameters
    */

    bool setEnabled(bool val = true);

};

#endif /* end of include guard: FORTYFIFTYONE_H */
